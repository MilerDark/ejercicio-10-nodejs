const express = require('express');
const axios = require('axios');
const app = express();
const port = 8080;
const url = 'https://api.covidtracking.com/v2/us/daily.json';

app.get('/', async (req, res) => {
 
   try {
    const jsonResponse = await axios.get(url);
    const response = jsonResponse.data;
    res.status(200).json({
     response
    });
   } catch (error) {
      console.log(error);
  
    res.status.json({
     error: 'Hubo un error al consumir el servicio'
    });
   }
  });
  app.listen(port, () => {
   console.log(`servidor corriendo en el puerto ${port}`);
});

